public enum Tiles{
	BLANK("_"),
	X("X"),
	O("O");
	
	private String name;
	
	
	private Tiles (String name){
		this.name = name;
	}

	public String getName(){
		return this.name;
	}
}
