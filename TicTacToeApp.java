import java.util.Scanner;
import java.util.Random;
public class TicTacToeApp{
	public static void main(String[] args){
		System.out.println(" Hello dear player!!! \n Welcome to the game of Tic Tac Toe. \n We are very happy to have you here. \n Play a game with our AI to get started ;)");
		System.out.println(Tiles.values().length);
		Board board = new Board();
		boolean gameOver = false;
		int player = 1;
		Tiles playerToken = Tiles.X;
		
		while(gameOver == false){
			Scanner reader=new Scanner(System.in);
			System.out.println(board);
			System.out.println("Input the ROW you would like to put your token. [0-2]");
			int row = reader.nextInt();
			
			System.out.println("Input the COLUMN you would like to put your token. [0-2]");
			int col = reader.nextInt();
			
			if(player == 1){  
				playerToken = Tiles.X;
			} else if(player == 0){
				playerToken = Tiles.O;
			}   
			
			System.out.println("Player 1's token: X");
			System.out.println("AI's token: O");
			
			player = 1;
			playerToken = Tiles.X;
			boolean tokenIsPlaced = board.placeToken(row, col, playerToken);
			while(tokenIsPlaced == false){
				System.out.println("Please re-enter a valid position [0-2]");
				
				System.out.println("Input the ROW you would like to put your token.");
				row = reader.nextInt();
			
				System.out.println("Input the COLUMN you would like to put your token.");
				col = reader.nextInt();
			
				tokenIsPlaced = board.placeToken(row, col, playerToken);
			}
			
			if(board.checkIfWinning(playerToken) == true){
				System.out.println("Player " + player + " is the winner!!!");
				gameOver = true;
				System.out.println("Would you like to continue with another round? ENTER 1 TO CONTINUE OR ENTER ANY OTHER NUMBER TO END THE GAME");

				int nextRound = reader.nextInt();
				if(nextRound == 1){
					gameOver = false;
				}else{
					gameOver = true;
				}
			} else if(board.checkIfFull() == true){
				System.out.println("It's a tie!");
				gameOver = true;

				player = player+1;
				if(player > 2){
					player = 1;	
				}	
			}
			
			//AI's code version
			player = 0;
			playerToken = Tiles.O;
			if(player == 0){
				Random random = new Random();
				int randomRow = random.nextInt(Tiles.values().length);
				int randomCol = random.nextInt(Tiles.values().length);
				System.out.println("AI has chosen " +randomRow+" as it's ROW" );
				System.out.println("AI has chosen " +randomCol+" as it's COLUMN" );
				
				tokenIsPlaced = board.placeToken(randomRow, randomCol, playerToken);
				while(tokenIsPlaced == false){
					System.out.println("Please re-enter a valid position [0-2]");
					
					System.out.println("Input the ROW and COLUMN you would like to put your token in again.");
					randomRow = random.nextInt(Tiles.values().length);
					randomCol = random.nextInt(Tiles.values().length);
					System.out.println("AI has chosen " +randomRow+" as it's ROW" );
					System.out.println("AI has chosen " +randomCol+" as it's COLUMN" );
				
					tokenIsPlaced = board.placeToken(randomRow, randomCol, playerToken);
				}
				
			}
			
			if(board.checkIfWinning(playerToken) == true){
				System.out.println("Player " + player + " is the winner!!!");
				gameOver = true;
				System.out.println("Would you like to continue with another round? ENTER 1 TO CONTINUE OR ENTER ANY OTHER NUMBER TO END THE GAME");

				int nextRound = reader.nextInt();
				if(nextRound == 1){
					gameOver = false;
				}else{
					gameOver = true;
				}
			} else if(board.checkIfFull() == true){
				System.out.println("It's a tie!");
				gameOver = true;

				player = player+1;
				if(player > 2){
					player = 1;	
				}	
				System.out.println("WOULD YOU LIKE TO CONTINUE WITH A NEW ROUND? ENTER 1 TO CONTINUE OR ENTER ANY OTHER NUMBER TO END THE GAME");
				//is currently continuing previous game so we gotta get it to restart.
				
				int nextRound = reader.nextInt();
				if(nextRound == 1){
					board = new Board();
					gameOver = false;
				}else{
					gameOver = true;
				}
			}
			
		}
		
	}
}