public class Board{
	private Tiles[][] grid;
	
	public Board(){
		this.grid = new Tiles [Tiles.values().length][Tiles.values().length];
		for(int i = 0; i<Tiles.values().length; i++){
			for(int x = 0; x <Tiles.values().length; x++){
				this.grid[i][x] = Tiles.BLANK;
			}
		}
	}
	
	public String toString(){
		String arr = "";
		for(Tiles[] x: this.grid){
			arr+="\n";
			for(Tiles y: x){
				arr+=y.getName() + " ";
			}
		}
		return arr;
	}
	
	public boolean placeToken(int row, int col, Tiles playerToken){
		boolean tokenIsPlaced = false;
		if(row >= 0 && row <3 && col >= 0 && col <3 ){
			if(this.grid[row][col] == Tiles.BLANK){
				this.grid[row][col] = playerToken;
				tokenIsPlaced = true;
			}
			else{
				tokenIsPlaced = false;
			}
		}
		return tokenIsPlaced;
	}
	
	public boolean checkIfFull(){
		boolean isFull = false;
		for(Tiles[] x : this.grid){
			for(Tiles y : x){
				if(y == Tiles.BLANK){
					isFull = true;
				}
			}
		}
		return isFull;
	}
	
	private boolean checkIfWinningHorizontal(Tiles playerToken){
		 for(int row = 0; row < 3; row++){
			if(this.grid[row][0] == playerToken && this.grid[row][1] == playerToken && this.grid[row][2] == playerToken){
				return true;
			}
		}
		return false;
	}
	
	private boolean checkIfWinningVertical(Tiles playerToken){
		for (int col = 0; col < 3; col++) {
            if (this.grid[0][col] == playerToken && this.grid[1][col] == playerToken && this.grid[2][col] == playerToken) {
                return true;
            }
        }
		return false;
	}
	
	private boolean checkIfWinningDiagonal(Tiles playerToken){
			if (this.grid[0][2] == playerToken && this.grid[1][1] == playerToken && this.grid[2][0] == playerToken) {
				return true; 
				
			}
			if (this.grid[0][0] == playerToken && this.grid[1][1] == playerToken && this.grid[2][2] == playerToken) {
				return true; 
			}
		return false; 
	}
	
	public boolean checkIfWinning(Tiles playerToken){
		boolean isWinning = false;
		if((checkIfWinningHorizontal(playerToken) == true) || (checkIfWinningVertical(playerToken) == true) || (checkIfWinningDiagonal(playerToken) == true)){
			isWinning = true;
		}
		return isWinning;
	} 
	
}